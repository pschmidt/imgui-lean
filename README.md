# imgui-lean

Fork of imgui with easy-to-use CMakeLists.txt

Also includes
 * ImGuizmo: https://github.com/CedricGuillemet/ImGuizmo
 * ImPlot: https://github.com/epezent/implot
